package com.example.snikerssss.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.snikerssss.R
import org.w3c.dom.Text

class DashboardFragment: Fragment(R.layout.fragment_dashboard) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.textView).text =
        DashboardFragmentArgs.fromBundle(requireArguments()).amount.toString()
    }
}